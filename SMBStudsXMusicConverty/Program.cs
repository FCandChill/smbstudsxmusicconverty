﻿using System;
using System.Collections.Generic;
using System.IO;

namespace SMB1MusicConverty
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 2 || args.Length == 3)
            {
                if (args.Length == 3 && args[2] == "/Verbose")
                {
                    Master.Verbose = true;
                }

                string SongPath = args[0];
                string DirectoryPath = Path.GetDirectoryName(SongPath);

                if (args[1] != "all")
                {
                    foreach (string s in args[1].Split(','))
                    {
                        if (!int.TryParse(s, out int i))
                        {
                            throw new ArgumentException($"Bad song id argument: {s}");
                        }

                        Master.SongsToProcess.Add(i);
                    }
                }

                ReadFtmTXT.Read(SongPath);
                ConvertToSMB1 smb1 = new ConvertToSMB1();
                smb1.Convert(out Dictionary<int, string> ASMOutputS, out Dictionary<int, string> DebugOutputS);

                foreach(var kvp in ASMOutputS)
                {
                    File.WriteAllText(Path.Combine(DirectoryPath, $"zzz_Output_{kvp.Key + 1:000}.asm"), kvp.Value);
                }

                if (Master.Verbose)
                {
                    foreach (var kvp in DebugOutputS)
                    {
                        File.WriteAllText(Path.Combine(DirectoryPath, $"zzz_DebugOutput_{kvp.Key + 1:000}.asm"), kvp.Value);
                    }
                }
            }
            else
            {
                Console.WriteLine("Invalid arguments supplied.");
                Console.WriteLine("Usage:");
                Console.WriteLine(@"SMBStudXMusicConverty.exe TXTFILE.txt TRACK# /Verbose");
            }
        }
    }
}