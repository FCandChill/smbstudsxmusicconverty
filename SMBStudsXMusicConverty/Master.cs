﻿using SMB1MusicConverty;
using System.Collections.Generic;
using System.IO;

public static class Master
{
    public static Dictionary<int,Dictionary<Enums.Channel, Dictionary<int, Dictionary<int, SequenceLine>>>> Channel = new Dictionary<int, Dictionary<Enums.Channel, Dictionary<int, Dictionary<int, SequenceLine>>>>();
    public static Dictionary<int,Dictionary<Enums.Channel, Dictionary<int, int>>> Order = new Dictionary<int, Dictionary<Enums.Channel, Dictionary<int, int>>>();
    public static Dictionary<int, int> MeasureLength = new Dictionary<int, int>();

    public static Dictionary<int, byte[]> VRC7Instruments = new Dictionary<int, byte[]>();

    public static List<int> SongsToProcess = new List<int>();

    public static int Speed;
    public static bool Verbose = false;
    public static string GeneratSequenceDebugLine(Enums.Channel Channel, int Measure, int SequenceLineNo, SequenceLine SequenceLine)
    {
        string Prefix = SequenceLine.Effects.Count == 1 ? SequenceLine.Effects[0].EffectPrefix : ".";
        string Argument = SequenceLine.Effects.Count == 1 ? SequenceLine.Effects[0].Argument.ToString("X2") : "..";
        string Note = SequenceLine.Note != "" ? SequenceLine.Note : "...";
        string Instrument = SequenceLine.Instrument != Constants.NULL ? SequenceLine.Instrument.ToString("X2") : "..";
        string Volume = SequenceLine.Volume != Constants.NULL ? SequenceLine.Volume.ToString("X") : ".";
        string Length = SequenceLine.Length != Constants.NULL ? SequenceLine.Length.ToString("00") : "--";

        return ($"Channel: {Channel,-8}; Part: {Measure + 1:X2}; Measure: {Measure:X2}; Sequence: {SequenceLineNo:X2}; Line: {Note} {Instrument} {Volume} {Prefix}{Argument} x{Length}");

    }
}
