﻿public struct Effect
{
    public string EffectPrefix { get; private set; }
    public int Argument { get; private set; }

    public Effect(string Effect_Prefix, string Argument)
    {
        this.EffectPrefix = Effect_Prefix;
        this.Argument = MyConvert.HexToDec(Argument);
    }
}