﻿using System.Collections.Generic;
public class SequenceLine
{
    public string Note { get; set; }
    public int Instrument { get; set; }
    public int Volume { get; set; }
    public int Length { get; set; }
    public List<Effect> Effects { get; set; }
    public SequenceLine(string Note, string Instrument, string Volume, List<Effect> Effects)
    {
        this.Note = Note;
        this.Volume = MyConvert.HexToDec(Volume);
        this.Instrument = MyConvert.HexToDec(Instrument);
        this.Effects = Effects;
        this.Length = Constants.NULL;
    }

    public SequenceLine()
    {
        this.Note = "";
        this.Volume = Constants.NULL;
        this.Instrument = Constants.NULL;
        this.Effects = new List<Effect>();
        this.Length = Constants.LENGTH_NULL;
    }

    public SequenceLine(string Note, int Instrument, int Volume, List<Effect> Effects)
    {
        this.Note = Note;
        this.Volume = Volume;
        this.Instrument = Instrument;
        this.Effects = Effects;
        this.Length = Constants.LENGTH_NULL;
    }

    public SequenceLine Copy() => new SequenceLine(Note, Instrument, Volume, Effects);
}