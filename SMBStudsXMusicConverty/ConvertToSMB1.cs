﻿using SMB1MusicConverty;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class ConvertToSMB1
{
    readonly Dictionary<int, StringBuilder> ASMOutput = new Dictionary<int, StringBuilder>();
    readonly Dictionary<int, StringBuilder> DebugOutput = new Dictionary<int, StringBuilder>();

    int SongId;
    public void Convert(out Dictionary<int, string> ASMOutputS, out Dictionary<int, string> DebugOutputS)
    {
        ASMOutputS = new Dictionary<int, string>();
        DebugOutputS = new Dictionary<int, string>();

        foreach (int SongId_ in Master.SongsToProcess)
        {
            SongId = SongId_;

            ASMOutput.Add(SongId, new StringBuilder());
            DebugOutput.Add(SongId, new StringBuilder());

            void PrintSeperator()
            {
                if (Master.Verbose)
                {
                    string s = $"==========================================================================";

                    DebugOutput[SongId].AppendLine(s);
                    Console.WriteLine(s);
                }
            }

            foreach (var Channel in Master.Channel[SongId])
            {
                PrintSeperator();

                switch (Channel.Key)
                {
                    case Enums.Channel.Square1:
                    case Enums.Channel.Square2:
                    case Enums.Channel.Triangle:
                        continue;
                    case Enums.Channel.Noise:
                        ASMOutput[SongId].AppendLine(Environment.NewLine + "conv_Noi:");
                        break;
                    case Enums.Channel.DPCM:
                        ASMOutput[SongId].Append(Environment.NewLine + "conv_DPCM:");
                        break;
                    case Enums.Channel.FMCh1:
                        ASMOutput[SongId].Append(Environment.NewLine + Environment.NewLine + "conv_VRC1:");
                        break;
                    case Enums.Channel.FMCh2:
                        ASMOutput[SongId].Append(Environment.NewLine + Environment.NewLine + "conv_VRC2:");
                        break;
                    case Enums.Channel.FMCh3:
                        ASMOutput[SongId].Append(Environment.NewLine + Environment.NewLine + "conv_VRC3:");
                        break;
                    case Enums.Channel.FMCh4:
                        ASMOutput[SongId].Append(Environment.NewLine + Environment.NewLine + "conv_VRC4:");
                        break;
                    case Enums.Channel.FMCh5:
                        ASMOutput[SongId].Append(Environment.NewLine + Environment.NewLine + "conv_VRC5:");
                        break;
                    case Enums.Channel.FMCh6:
                        ASMOutput[SongId].Append(Environment.NewLine + Environment.NewLine + "conv_VRC6:");
                        break;
                }

                SequenceLine Current = new SequenceLine();
                SequenceLine Previous = new SequenceLine();

                List<string> ConvertedNoiseNotes;
                foreach (var Measure in Channel.Value)
                {
                    PrintSeperator();

                    ASMOutput[SongId].AppendLine(Environment.NewLine + Environment.NewLine + $"{Constants.INDENT};part {Measure.Key + 1}");

                    // Add a dummy sequence line. This is a hack so the actual last line is inserted
                    if (Measure.Value.Count > 0)
                    {
                        Measure.Value.Add(Measure.Value.Keys.Max() + 1, new SequenceLine(Constants.NA, -1, -1, new List<Effect>() { }));
                    }

                    ConvertedNoiseNotes = new List<string>();

                    foreach (var SequenceLine in Measure.Value)
                    {
                        bool Proccess = true;

                        if (SequenceLine.Value.Length == 0)
                        {
                            Proccess = false;
                        }
                        else
                        {
                            Previous = Current;
                            Current = SequenceLine.Value;
                        }

                        if (Current.Volume == Constants.NULL && Previous.Volume != Constants.NULL)
                        {
                            Current.Volume = Previous.Volume;
                        }
                        else if (Current.Volume == Constants.NULL && Previous.Volume == Constants.NULL && Current.Note != "")
                        {
                            Current.Volume = 0xF;
                        }

                        if (Current.Instrument == Constants.NULL && Previous.Instrument != Constants.NULL)
                        {
                            Current.Instrument = Previous.Instrument;
                        }

                        if (Master.Verbose && SequenceLine.Value.Note != Constants.NA)
                        {
                            string s = Master.GeneratSequenceDebugLine(Channel.Key, Measure.Key, SequenceLine.Key, SequenceLine.Value);

                            DebugOutput[SongId].AppendLine(s);
                            Console.WriteLine(s);
                        }

                        if (Channel.Key == Enums.Channel.FMCh3)
                        {

                        }

                        bool LengthChanged = Current.Length != Previous.Length;
                        bool VolumeChanged = (Current.Volume != Previous.Volume);
                        bool InstrumentChanged = Current.Instrument != Previous.Instrument;

                        if (Channel.Key == Enums.Channel.FMCh1)
                        {

                        }

                        if (Proccess)
                        {
                            switch (Channel.Key)
                            {
                                case Enums.Channel.Noise:
                                    ConvertedNoiseNotes.Add(ProcessNoiseChannel(Current));
                                    break;
                                case Enums.Channel.DPCM:
                                    ProcessDPCMChannel(Current, LengthChanged);
                                    break;
                                case Enums.Channel.FMCh1:
                                case Enums.Channel.FMCh2:
                                case Enums.Channel.FMCh3:
                                case Enums.Channel.FMCh4:
                                case Enums.Channel.FMCh5:
                                case Enums.Channel.FMCh6:
                                    ProcessVRC7Channels(Current, LengthChanged, VolumeChanged, InstrumentChanged);
                                    break;
                                default:
                                    throw new Exception();
                            }
                        }
                    }

                    if (ConvertedNoiseNotes.Count > 0)
                    {
                        ASMOutput[SongId].AppendLine($"{Constants.INDENT}{Constants.DB} {string.Join(", ", ConvertedNoiseNotes)}");
                    }
                }
            }
        }

        foreach(var kvp in Master.SongsToProcess)
        {
            ASMOutputS.Add(kvp, ASMOutput[kvp].ToString().Replace($"{Environment.NewLine},", $"{Environment.NewLine}{Constants.INDENT}{Constants.DB}"));
            DebugOutputS.Add(kvp, DebugOutput[kvp].ToString());
        }
    }

    private string ProcessNoiseChannel(SequenceLine h)
    {
        int InstrumentNybble;

        if (h.Instrument >= 0x30)
        {
            InstrumentNybble = h.Instrument - 0x30;
        } 
        else if (h.Instrument == Constants.NULL)
        {
            InstrumentNybble = 0x0;
        }
        else
        {
            throw new Exception($"Instrument of range: {h.Instrument}.");
        }

        int Length = h.Length;
        while (Length > 0xF)
        {
            ASMOutput[SongId].AppendLine($"{Constants.INDENT}{Constants.DB} ${((0xF) << 4) | (0x0):X2}");
            Length -= 0xF;
        }

        return $"${((Length & 0xF) << 4) | (InstrumentNybble & 0xF):X2}";
    }

    private void ProcessDPCMChannel(SequenceLine h, bool LengthChanged)
    {
        if (LengthChanged)
        {
            int Length = h.Length;

            if (Length >= 0xF)
            {
                ASMOutput[SongId].AppendLine($"{Constants.INDENT}{Constants.DB} ${((0xF) << 4) | (0x80):X2}");

                bool First = true;

                while (Length > 0xF)
                {
                    if (!First)
                    {
                        ASMOutput[SongId].Append($", ");
                    }
                    else
                    {
                        ASMOutput[SongId].Append($"{Constants.INDENT}{Constants.DB} ");
                        First = false;
                    }
                    ASMOutput[SongId].Append($"{CreateNote(h, "N")}");
                    Length -= 0xF;
                }
            }

            ASMOutput[SongId].AppendLine($"");
            ASMOutput[SongId].AppendLine($"{Constants.INDENT}{Constants.DB} ${Length | 0x80:X2}");
            ASMOutput[SongId].Append($"{Constants.INDENT}{Constants.DB} ");
        }
        else
        {
            ASMOutput[SongId].Append($", ");
        }

        ASMOutput[SongId].Append(CreateNote(h, "N"));
    }

    private void ProcessVRC7Channels(SequenceLine h, bool LengthChanged, bool VolumeChanged, bool InstrumentChanged)
    {
        void PrintEffectLabel()
        {
            ASMOutput[SongId].AppendLine($"");
            ASMOutput[SongId].Append($"{Constants.INDENT}{Constants.DB} ${00:X2}");
        }

        if (h.Effects.Count > 0)
        {
            if (h.Effects[0].EffectPrefix != "D")
            {
                PrintEffectLabel();
            }

            switch (h.Effects[0].EffectPrefix)
            {
                case "B":
                    ASMOutput[SongId].AppendLine($", ${0x00:X2}");
                    break;
                case "P":
                    if (h.Effects[0].Argument < 0x80)
                    {
                        ASMOutput[SongId].Append($", ${0x05:X2}, ${h.Effects[0].Argument:X2}-$81");
                    }
                    else
                    {
                        ASMOutput[SongId].Append($", ${0x05:X2}, ${h.Effects[0].Argument:X2}-$80");
                    }
                    break;
                case "Q":
                    if (h.Effects[0].Argument > 0x80)
                    {
                        throw new Exception($"Effect Q argument out of range: {h.Effects[0].Argument:X2}.");
                    }

                    ASMOutput[SongId].Append($", ${0x06:X2}, ${h.Effects[0].Argument | 0x80:X2}");

                    break;
                case "1":

                    if (h.Effects[0].Argument > 0x80)
                    {
                        throw new Exception($"Effect 1 argument out of range: {h.Effects[0].Argument:X2}.");
                    }

                    ASMOutput[SongId].Append($", ${0x07:X2}, ${h.Effects[0].Argument | 0x80:X2}");

                    break;
                case "2":

                    if (h.Effects[0].Argument > 0x80)
                    {
                        throw new Exception($"Effect 2 argument out of range: {h.Effects[0].Argument:X2}.");
                    }

                    ASMOutput[SongId].Append($", ${0x07:X2}, ${h.Effects[0].Argument:X2}");

                    break;
                case "0":
                    ASMOutput[SongId].Append($", ${0x08:X2}, ${h.Effects[0].Argument:X2}");
                    break;
                case "C":
                    ASMOutput[SongId].Append($", ${0x09:X2}");
                    break;
                case "D":

                    if (h.Effects[0].Argument != 0x00)
                    {
                        throw new Exception($"Effect D argument out of range: {h.Effects[0].Argument:X2}.");
                    }

                    break;
                default:
                    Console.WriteLine($"Unknown SFX prefix: {h.Effects[0].EffectPrefix}");
                    break;
            }
        }

        if (VolumeChanged || InstrumentChanged)
        {
            int Instrument = h.Instrument;
            if (h.Instrument > 0xF || Instrument == -1)
            {
                Instrument = 0;
            }

            int Volume = 0xF - h.Volume;

            PrintEffectLabel();
            ASMOutput[SongId].Append($", ${0x02:X2}, ${Instrument << 4 | Volume:X2}");
        }

        if (InstrumentChanged && h.Instrument > 0xF)
        {
            byte[] Instrument = Master.VRC7Instruments[h.Instrument];

            // Stupid hack
            PrintEffectLabel();
            ASMOutput[SongId].Append($", ${0x03:X2}, ${Instrument[0]:X2}, ${Instrument[1]:X2}, ${Instrument[2]:X2}, ${Instrument[3]:X2}, ${Instrument[4]:X2}, ${Instrument[5]:X2}, ${Instrument[6]:X2}, ${Instrument[7]:X2}");
        }

        int Length = h.Length;

        if (Length > 0xFF)
        {
            ASMOutput[SongId].AppendLine($"{Environment.NewLine}{Constants.INDENT}{Constants.DB} ${0x01:X2}, ${0xFF:X2}");

            while (Length > 0xFF)
            {
                ASMOutput[SongId].AppendLine($"{Constants.INDENT}{Constants.DB} {"VC_"}{"Sil"}");
                Length -= 0xFF;
            }
        }

        if (LengthChanged)
        {
            ASMOutput[SongId].AppendLine($"{Environment.NewLine}{Constants.INDENT}{Constants.DB} ${0x01:X2}, ${Length:X2}");
            ASMOutput[SongId].Append($"{Constants.INDENT}{Constants.DB}");
        }
        else
        {
            ASMOutput[SongId].Append($",");
        }

        ASMOutput[SongId].Append($" {CreateNote(h, "VC")}");
    }

    public string CreateNote(SequenceLine h, string Prefix)
    {
        string Label;

        if (h.Note == "")
        {
            Label = "Sil";
        }
        else if (h.Note == "---")
        {
            Label = "Halt";
        }
        else if (h.Note.Contains("-"))
        {
            Label = h.Note.Replace("-", "");
        }
        else if (h.Note.Contains("#"))
        {
            Label = h.Note.Replace("#", "f");
        }
        else if (h.Note == "===")
        {
            Label = "Rel";
        }
        else
        {
            throw new Exception($"Invalid note: {h.Note}");
        }

        return $"{Prefix}_{Label}";
    }
}
