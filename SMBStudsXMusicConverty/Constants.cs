﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public static class Constants
{
    public const int NULL = -1;
    public const int LENGTH_NULL = 0;

    public const int b_NULL = 0xFF;
    public const int Sound_Channels = 11;

    public const string DB = ".db";
    public const string NA = "N/A";
    public const string INDENT = "		";


}