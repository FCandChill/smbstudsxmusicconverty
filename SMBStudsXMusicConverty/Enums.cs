﻿namespace SMB1MusicConverty
{
    public class Enums
    {
        public enum PatternColumnInfo
        {
            Note,
            Instrument,
            Volume
        }

        public enum Channel
        {
            Square1,
            Square2,
            Triangle,
            Noise,
            DPCM,
            FMCh1,
            FMCh2,
            FMCh3,
            FMCh4,
            FMCh5,
            FMCh6
        }
    }
}