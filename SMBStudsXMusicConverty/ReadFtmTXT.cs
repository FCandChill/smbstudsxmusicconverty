﻿using SMB1MusicConverty;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

public class ReadFtmTXT
{
    public static void Read(string Path)
    {
        var filestream = new System.IO.FileStream(Path,
                              System.IO.FileMode.Open,
                              System.IO.FileAccess.Read,
                              System.IO.FileShare.ReadWrite);

        var file = new System.IO.StreamReader(filestream, System.Text.Encoding.UTF8, true, 128);
        bool FoundTracksHeader = false;
        bool PatternRead = false;
        string lineOfText;
        int SequenceNumber = 0;
        int PatternNo = 0;
        int SongId = -1;

        while ((lineOfText = file.ReadLine()) != null)
        {
            if (lineOfText == "# Tracks")
            {
                FoundTracksHeader = true;
                continue;
            }
            else if (lineOfText.StartsWith("TRACK "))
            {
                string[] Splitted = lineOfText.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);

                Master.Speed = int.Parse(Splitted[2]);

                SongId++;

                Console.WriteLine($"{SongId + 1:000}: {lineOfText}");

                Master.MeasureLength.Add(SongId, int.Parse(Splitted[1]));
                Master.Channel.Add(SongId, new Dictionary<Enums.Channel, Dictionary<int, Dictionary<int, SequenceLine>>>());
                Master.Order.Add(SongId, new Dictionary<Enums.Channel, Dictionary<int, int>>());

                for (int i = 0; i < Enum.GetNames(typeof(Enums.Channel)).Length; i++)
                {
                    Master.Channel[SongId].Add((Enums.Channel)i, new Dictionary<int, Dictionary<int, SequenceLine>>());
                    Master.Order[SongId].Add((Enums.Channel)i, new Dictionary<int, int>());
                }
            }
            // Read VRC7 instrument data specofic for this sound engine
            else if (lineOfText.Contains("INSTVRC7"))
            {
                lineOfText = new Regex(@"INSTVRC7\s+").Replace(lineOfText, "");
                lineOfText = new Regex(@"\s"".*""").Replace(lineOfText, "");
                string[] Splitted = lineOfText.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);

                string[] StrToConvert = new string[Splitted.Length - 2];
                Array.Copy(Splitted, 2, StrToConvert, 0, StrToConvert.Length);

                List<byte> Bytes = new List<byte>();

                foreach (string s in StrToConvert)
                {
                    Bytes.Add((byte)MyConvert.HexToDec(s));
                }

                Master.VRC7Instruments.Add(int.Parse(Splitted[0]), Bytes.ToArray());
            }
            else if (lineOfText.Contains("ORDER "))
            {
                lineOfText = new Regex(@"ORDER\s+").Replace(lineOfText, "");
                lineOfText = new Regex(@"\s"".*""").Replace(lineOfText, "");
                lineOfText = new Regex(@":").Replace(lineOfText, "");

                string[] Splitted = lineOfText.Split(new string[] { " " }, StringSplitOptions.RemoveEmptyEntries);

                string[] StrToConvert = new string[Splitted.Length - 1];
                Array.Copy(Splitted, 1, StrToConvert, 0, StrToConvert.Length);

                int Channel = 0;
                foreach (string s in StrToConvert)
                {
                    Master.Order[SongId][(Enums.Channel)Channel++].Add(MyConvert.HexToDec(Splitted[0]), MyConvert.HexToDec(s));
                }
            }
            else if (FoundTracksHeader && !PatternRead)
            {
                if (lineOfText.Contains("PATTERN "))
                {
                    PatternRead = true;
                    int Length = "PATTERN ".Length;
                    PatternNo = MyConvert.HexToDec(lineOfText.Substring(Length, lineOfText.Length - Length));
                    SequenceNumber = 0;

                    for (int i = 0; i < Enum.GetNames(typeof(Enums.Channel)).Length; i++)
                    {
                        Master.Channel[SongId][(Enums.Channel)i].Add(PatternNo, new Dictionary<int, SequenceLine>());
                    }

                    continue;
                }
            }
            else if (PatternRead)
            {
                if (!string.IsNullOrEmpty(lineOfText))
                {
                    string[] Splitted_ = lineOfText.Split(new[] { " : " }, StringSplitOptions.RemoveEmptyEntries);
                    string[] Splitted = new string[Splitted_.Length - 1];

                    Array.Copy(Splitted_, 1, Splitted, 0, Splitted.Length);

                    int SoundChannel = 0;

                    foreach (string Entry in Splitted)
                    {
                        string[] Section = Entry.Split(' ');
                        string Note = "", Instrument = "", Volume = "";
                        List<Effect> l_effect = new List<Effect>();
                        for (int i = 0; i < Section.Length; i++)
                        {
                            string Sanitize(string s) => s.Contains(".") ? "" : s;
                            string Index = Section[i];
                            switch (i)
                            {
                                case 0:
                                    Note = Sanitize(Index);
                                    break;
                                case 1:
                                    Instrument = Sanitize(Index);
                                    break;
                                case 2:
                                    Volume = Sanitize(Index);
                                    break;
                                default:
                                    string EffectName, Argument;
                                    if (Section[i] != "...")
                                    {
                                        EffectName = Index[0].ToString();
                                        Argument = Index.Substring(1);
                                        l_effect.Add(new Effect(EffectName, Argument));
                                    }
                                    break;
                            }
                        }

                        Master.Channel[SongId][(Enums.Channel)SoundChannel++][PatternNo].Add(SequenceNumber, new SequenceLine(Note, Instrument, Volume, l_effect));
                    }
                    SequenceNumber++;
                }
                else
                {
                    PatternRead = false;
                }
            }
        }

        if (Master.SongsToProcess.Count == 0)
        {
            foreach (int i in Master.Channel.Keys)
            {
                Master.SongsToProcess.Add(i);
            }
        }

        Dictionary<int, Dictionary<Enums.Channel, Dictionary<int, Dictionary<int, SequenceLine>>>> Copy = new Dictionary<int, Dictionary<Enums.Channel, Dictionary<int, Dictionary<int, SequenceLine>>>>();

        foreach (int SId in Master.SongsToProcess)
        {
            Copy.Add(SId, new Dictionary<Enums.Channel, Dictionary<int, Dictionary<int, SequenceLine>>>());

            if (!Master.Order.ContainsKey(SId))
            {
                throw new Exception($"Invalid song id: {SId}");
            }

            foreach (var Channel in Master.Order[SId])
            {
                Copy[SId].Add(Channel.Key, new Dictionary<int, Dictionary<int, SequenceLine>>());

                SequenceLine Current = new SequenceLine();
                SequenceLine Previous = new SequenceLine();

                foreach (var Measurex in Channel.Value)
                {
                    if (Channel.Key == Enums.Channel.FMCh1 && Measurex.Key == 0)
                    {

                    }

                    // Famitracker has this weird feature where if a measure ID isn't used at all,
                    // it's not included in the TXT export
                    // As a result, this is a workaround, because otherwise, 
                    // a measure will be printed as blank.
                    if (!Master.Channel[SId][Channel.Key].ContainsKey(Measurex.Value))
                    {
                        Master.Channel[SId][Channel.Key].Add(Measurex.Value, new Dictionary<int, SequenceLine>());
                        
                        for(int i = 0; i < Master.MeasureLength[SId]; i++)
                        {
                            Master.Channel[SId][Channel.Key][Measurex.Value].Add(i, new SequenceLine());
                        }
                    }

                    var Measure = new Dictionary<int, SequenceLine>(Master.Channel[SId][Channel.Key][Measurex.Value]);

                    Copy[SId][Channel.Key].Add(Measurex.Key, new Dictionary<int, SequenceLine>());

                    Measure.Add(Measure.Keys.Max() + 1, new SequenceLine(Constants.NA, Constants.NULL, Constants.NULL, new List<Effect>() { }));

                    if (Master.Verbose)
                    {
                        Console.WriteLine($"==========================================================================");
                    }

                    foreach (var SequenceLinex in Measure)
                    {
                        if (Channel.Key == Enums.Channel.FMCh1)
                        {

                        }

                        var SequenceLine = SequenceLinex.Value.Copy();
                        if (Master.Verbose && SequenceLine.Note != Constants.NA)
                        {
                            Console.WriteLine(Master.GeneratSequenceDebugLine(Channel.Key, Measurex.Key, SequenceLinex.Key, SequenceLine));
                        }

                        bool LengthChanged = Current.Length != Previous.Length;
                        bool NoteBlank = SequenceLine.Note == "" && SequenceLine.Length == Constants.LENGTH_NULL;
                        bool ContainsEffect = SequenceLine.Effects.Count != 0;
                        bool VolumeChanged = Current.Volume != SequenceLine.Volume && SequenceLine.Volume != Constants.NULL;
                        bool InstrumentChanged = Current.Instrument != SequenceLine.Instrument && SequenceLine.Instrument != Constants.NULL;

                        bool Process = !NoteBlank || ContainsEffect || VolumeChanged || InstrumentChanged;

                        // Don't ignore a blank space if it's at the beginning, otherwise,
                        // the timing will be off.
                        if (!Process && NoteBlank && Measurex.Key == 0 && SequenceLinex.Key == 0)
                        {
                            Previous = Current;
                            Current = SequenceLine;
                            Copy[SId][Channel.Key][Measurex.Key].Add(SequenceLinex.Key, SequenceLine);
                        }

                        if (Process)
                        {
                            bool Exit = false;
                            if (Current.Effects.Count > 0 && (Current.Effects[0].EffectPrefix == "C" || Current.Effects[0].EffectPrefix == "D"))
                            {
                                Current.Length = 1;
                                Exit = true;
                            }

                            if (SequenceLine.Note != Constants.NA)
                            {
                                Previous = Current;
                                Current = SequenceLine;

                                if (Current.Volume == Constants.NULL)
                                {
                                    Current.Volume = Previous.Volume;
                                }

                                Copy[SId][Channel.Key][Measurex.Key].Add(SequenceLinex.Key, Current);

                                // Exit the measure early if C or D effect are used.
                                // These effects will have to be used in each channel, or else the song will be desynced for the connverter
                                // The converter being aware of the effects in other channels is too much work.
                                if (Exit)
                                {
                                    break;
                                }
                            }
                            else
                            {
                                // The entire loop lags behind by one to determine a note's length.
                                // The N/A is a cheap hack to count the last sequeline line.
                                // As a result, the last line shouldn't be incorporated into a note's
                                // length
                                Current.Length--;
                            }

                        }
                        Current.Length++;
                    }
                }
            }
        }

        Master.Channel = Copy;
    }
}
